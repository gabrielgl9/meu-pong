$(document).ready(function () {

    moverBola();

    $(document).keydown(function (e) {
        //tecla de seta para cima
        if ((e.which === 38 || e.keyCode === 38) && ($("#p1").offset().top >= 80)) {

            //parseInt para tirar o px, fazer a subtração e incrementar
            val = parseInt($("#p1").offset().top);
            val = val - 50;
            $("#p1").css({"top": val});

            //alert($("#p1").offset().top +" "+ $("#p1").offset().left);
        }

        //tecla de seta para baixo
        if ((e.wich == 40 || e.keyCode == 40) && ($("#p1").offset().top <= 380)) {

            val = parseInt($("#p1").offset().top);
            val = val + 50;
            $("#p1").css({"top": val});
        }

        //tecla do W
        if ((e.wich == 87 || e.keyCode == 87) && ($("#p2").offset().top >= 80)) {

            val = parseInt($("#p2").offset().top);
            val = val - 50;
            $("#p2").css({"top": val});
        }

        // tecla do S
        if ((e.wich == 83 || e.keyCode == 83) && ($("#p2").offset().top <= 380)) {

            val = parseInt($("#p2").offset().top);
            val = val + 50;
            $("#p2").css({"top": val});
        }
    });

    function moverBola() {

        var verificaDistanciaHorizontal, verificaDistanciaVertical, movimentoHorizontal, movimentoVertical, posicaoX, posicaoY;

        movimentoHorizontal = 5; // sem rand
        movimentoVertical = parseInt(Math.random() * 3 + 1); //mudar formato do inicio; se colocar 5, a bolinha anda cruzadamente 


        // inicia o game aleatoriamente
        verificaDistanciaHorizontal = parseInt(Math.random() * 2);
        verificaDistanciaVertical = parseInt(Math.random() * 2);

        //coloca a bolinha no meio do campo
        var posicaoX = parseInt($('#campo').width() / 2 - $('#bola').width() / 2);
        var posicaoY = parseInt($('#campo').height() / 2 - $('#bola').height() / 2);

        //limite das bordas, caso passe disso, a bola ricocheteia
        limiteEsquerda = 26;
        limiteDireita = 1320;
        limiteCima = 30;
        limiteDebaixo = 585;

        //parecido com um while, ele executa essa function a cada intervalo 3
        play = window.setInterval(function () {
            posicaoX = verificaDistanciaHorizontal === 1 ? posicaoX + movimentoHorizontal : posicaoX - movimentoHorizontal; //incrementa casa da bolinha para direita ou esquerda
            posicaoY = verificaDistanciaVertical === 1 ? posicaoY + movimentoVertical : posicaoY - movimentoVertical; ////incrementa casa da bolinha para cima ou para baixo

            //se for gol, reseta
            if (posicaoX < limiteEsquerda) {
                $(".placarB").text(parseInt($(".placarB").text()) + 1);
                clearInterval(play);
                moverBola();
            }

            //se for gol, reseta
            if (posicaoX > limiteDireita) {
                $(".placarA").text(parseInt($(".placarA").text()) + 1);
                clearInterval(play);
                moverBola();
            }

            //ricocheteia
            if (posicaoY < limiteCima) {
                posicaoY = posicaoY + movimentoVertical * 2;
                verificaDistanciaVertical = 1;
            }

            //ricocheteia
            if (posicaoY > limiteDebaixo) {
                posicaoX = posicaoX + 5 * 2;
                verificaDistanciaVertical = 0;
            }

            //Raquete Player 1, ela ricocheteia
            if ((parseInt(posicaoX) === 1300) && (posicaoY >= $("#p1").offset().top) && (posicaoY <= $("#p1").offset().top + 179)) {

                posicaoX = posicaoX - movimentoHorizontal * 2;
                verificaDistanciaHorizontal = 0;
            }

            //Raquete Player 2, ela ricocheteia 
            if ((parseInt(posicaoX) === 45) && (posicaoY >= $("#p2").offset().top) && (posicaoY <= $("#p2").offset().top + 179)) {
                posicaoX = posicaoX + movimentoHorizontal * 2;
                verificaDistanciaHorizontal = 1;
            }

            //coloca a bolinha na casa definida
            $('#bola').css({'top': posicaoY + 'px', 'left': posicaoX + 'px'});

        }, 0.01);
    }

});

